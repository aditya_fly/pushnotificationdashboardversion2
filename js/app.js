/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    angular.module('Flyrobe', [
        'ngStorage',
        'ngJoyRide',
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',
        "ui.select",
         'ui.bootstrap',
         'angularjs-dropdown-multiselect',
         'ui'                 
    ])
})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad
