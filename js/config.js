/**
 * Flyrobe - Push Notification Dashboard
 *
 * Flyrobe theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: true
    });

    $stateProvider

        .state('login', {
            url: "/login",
            templateUrl: "views/pages/login/login.html",
            data: { pageTitle: 'Login', specialClass: 'gray-bg' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['css/pages/login.css']
                        }
                    ]);
                }
            }
        })

        /*later on remove it*/
        .state('login_two_columns', {
            url: "/login",
            templateUrl: "views/pages/login/login.html",
            data: { pageTitle: 'Login', specialClass: 'gray-bg'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['css/pages/login.css']
                        }
                    ]);
                }
            }
        })

        .state('table', {
            abstract: true,
            url: "/dashboards",
            data: { pageTitle: 'Dashboard', specialClass: 'gray-bg' },
            templateUrl: "views/common/content.html"
        })

        .state('table.segments', {
            url: "/segments",
            templateUrl: "views/pages/segments/segment.html",
            data: { pageTitle: 'Segments' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js',
                                'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('table.createSegment', {
            url: "/create-segment",
            templateUrl: "views/pages/segments/create-segment.html",
            data: { pageTitle: ' Create Segments' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        },
                        {
                            files: ['css/pages/segment/create-segment.css']
                        }
                    ]);
                }
            }
        })

        .state('home.email', {
            url: "/email",
            templateUrl: "views/pages/email/email.html",
            data: { pageTitle: 'Email' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('table.emailCreation', {
            url: "/create-email",
            templateUrl: "views/pages/email/create-email.html",
            data: { pageTitle: 'Email' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/pages/email/create-email.css']
                        },
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('table.updateSavedEmailTemp', {
            url: "/updateSavedEmailTemp/:template_name",
            templateUrl: "views/pages/email/updateSavedEmailTemplate.html",
            data: { pageTitle: 'Saved email Template', specialClass: 'gray-bg' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/pages/email/create-email.css']
                        },
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('table.cloneEmailSavedTemp', {
            url: "/cloneEmailSavedTemp/:template_name",
            templateUrl: "views/pages/email/cloneEmailSavedTemp.html",
            data: { pageTitle: 'Clone Of Email', specialClass: 'gray-bg' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/pages/email/create-email.css']
                        },
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })



        .state('updateSavedTemp', {
            url: "/updateSavedTemp/:template_name",
            templateUrl: "views/pages/push/updateSavedTemplate.html",
            data: { pageTitle: 'Saved Template', specialClass: 'gray-bg' }
        })

        .state('updateSavedSmsTemp', {
            url: "/updateSavedSmsTemp/:template_name",
            templateUrl: "views/pages/sms/updateSavedSmsTemplate.html",
            data: { pageTitle: 'Saved Sms Template', specialClass: 'gray-bg' },
        })
        .state('updateSavedEmailTemp', {
            url: "/updateSavedEmailTemp",
            templateUrl: "views/pages/email/updateSavedEmailTemplate.html",
            data: { pageTitle: 'Saved email Template', specialClass: 'gray-bg' },
        })

        .state('createPN', {
            url: "/create-pn",
            templateUrl: "views/pages/push/create-pn.html",
            data: { pageTitle: 'Send Notification', specialClass: 'gray-bg' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['css/pages/login.css']
                        }
                    ]);
                }
            }
        })

        .state('cloneSavedTemp', {
            url: "/cloneSavedTemp/:template_name",
            templateUrl: "views/pages/push/cloneSavedTemplate.html",
            data: { pageTitle: 'Clone Of Notification', specialClass: 'gray-bg' },
        })

        .state('cloneSmsSavedTemp', {
            url: "/cloneSmsSavedTemp/:template_name",
            templateUrl: "views/pages/sms/cloneSmsSavedTemp.html",
            data: { pageTitle: 'Clone Of Sms', specialClass: 'gray-bg' },
        })


        .state('home', {
            abstract: true,
            url: "/dashboards",
            data: { pageTitle: 'Dashboard', specialClass: 'gray-bg' },
            templateUrl: "views/common/content.html",
        })

        .state('home.homePage', {
            url: "/homePage",
            templateUrl: "views/pages/home/homePage.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name: 'angular-flot',
                            files: ['js/plugins/flot/jquery.flot.js', 'js/plugins/flot/jquery.flot.time.js', 'js/plugins/flot/jquery.flot.tooltip.min.js', 'js/plugins/flot/jquery.flot.spline.js', 'js/plugins/flot/jquery.flot.resize.js', 'js/plugins/flot/jquery.flot.pie.js', 'js/plugins/flot/curvedLines.js', 'js/plugins/flot/angular-flot.js',]
                        },
                        {
                            serie: true,
                            files: ['js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js', 'js/plugins/jvectormap/jquery-jvectormap-2.0.2.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js']
                        },
                        {
                            name: 'ui.checkbox',
                            files: ['js/bootstrap/angular-bootstrap-checkbox.js']
                        }
                    ]);
                }
            },
            data: { pageTitle: 'Homepage', specialClass: "mini-navbar" },
        })

        .state('home.documentation', {
            url: "/documentation",
            templateUrl: "views/pages/documentation/documentation.html",
            data: { pageTitle: 'Documentation', specialClass: "mini-navbar" },
        })

        .state('home.liveEvents', {
            url: "/liveEvents",
            templateUrl: "views/pages/liveevents/liveEvents.html",
            data: { pageTitle: 'Live Events' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        /*.state('home.Segments', {
            url: "/segments",
            templateUrl: "views/pages/segments/segmentsMain.html",
            data: { pageTitle: 'Segments' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })*/

        .state('home.campaigns', {
            url: "/Campaigns",
            templateUrl: "views/pages/campaigns/campaignsMain.html",
            data: { pageTitle: 'Campaigns' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('home.segmentCampaigns', {
            url: "/create-segment-campaign",
            templateUrl: "views/pages/campaigns/segmentCampaigns.html",
            data: { pageTitle: 'Segment Campaigns' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['js/plugins/moment/moment.min.js', 'css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        }

                    ]);
                }
            }
        })
        .state('home.oneTimeCampaigns', {
            url: "/create-one-time-campaign",
            templateUrl: "views/pages/campaigns/one-time-campaign.html",
            // templateUrl: "views/pages/campaigns/oneTimeCampaigns.html",
            data: { pageTitle: 'One Time Campaigns' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },

                        {
                            name: 'datePicker',
                            files: ['js/plugins/moment/moment.min.js', 'css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        }

                    ]);
                }
            }
        })
        .state('home.eventCampaigns', {
            url: "/create-event-campaign",
            // templateUrl: "views/pages/campaigns/eventCampaigns.html",
            templateUrl: "views/pages/campaigns/event-campaign.html",
            data: { pageTitle: 'Event Campaigns' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/moment/moment.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['js/plugins/moment/moment.min.js', 'css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        }

                    ]);
                }
            }
        })

        /*.state('home.createSegment', {
            url: "/createSegment",
            templateUrl: "views/pages/segments/createSegment.html",
            data: { pageTitle: ' Create Segments' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })*/

        .state('home.pushMain', {
            url: "/pushMain",
            templateUrl: "views/pages/push/pushMain.html",
            data: { pageTitle: 'Push Notification' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })
        .state('home.sms', {
            url: "/sms",
            templateUrl: "views/pages/sms/smsMain.html",
            data: { pageTitle: 'Sms' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })

        .state('smsCreation', {
            url: "/sms-template-creation",
            templateUrl: "views/pages/sms/createSms.html",
            data: { pageTitle: 'Sms' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js', 'css/plugins/footable/footable.core.css']
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js']
                        }
                    ]);
                }
            }
        })


}

angular
    .module('Flyrobe')
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });
