/**
 * Dashboard
 *
 * Main controller.js file
 *
 * Functions (controllers)
 *  - MainCtrl
 
/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl($state, $rootScope, $state,$localStorage) {

    mainVm = this;
    mainVm.table_page_size = 8;

    mainVm.checkCredentials     = checkCredentials;
    mainVm.logout               = logout;


    //staging
    {
        mainVm.urlList = {
            "node_url1": "http://node-staging.flyrobeapp.com:3000/api/v2/",
            "node_url2": "http://node-staging.flyrobeapp.com:3000/api/v1/",
            "acl_base": "https://node-staging.flyrobeapp.com:8002"
        };
    }

    //produciton
    /*{
        mainVm.urlList = {
            "node_url1": "http://node-staging.flyrobeapp.com:3000/api/v2/",
            "node_url2": "http://node-staging.flyrobeapp.com:3000/api/v1/",
            "acl_base": "https://node-staging.flyrobeapp.com:8002"
        };
    }*/


    mainVm.AuthorizationHeaderList={
        "Authorization_1":'1nhf2fhjjjh0hahzn',
        "Authorization_2":'1nhf2f48djh05mnd8'
    };

    mainVm.header1 = {
        'Content-Type': 'application/json',
        'Authorization': mainVm.AuthorizationHeaderList.Authorization_1
    };

    mainVm.header2 = {
        'Content-Type': 'application/json',
        'Authorization': mainVm.AuthorizationHeaderList.Authorization_2
    };


    if(window.localStorage.getItem('pnd_token')){
        $rootScope.roles = JSON.parse(window.localStorage.getItem('roles'));
        $rootScope.token = window.localStorage.getItem('pnd_token');
        mainVm.roles = $rootScope.roles;
    } else {
        $state.go('login');
    }

    if($localStorage.PND){
        mainVm.email = $localStorage.PND.email
    }

    function logout() {
        window.localStorage.removeItem('roles');
        window.localStorage.removeItem('pnd_token');
        delete $localStorage.PND;
        $state.go('login');
    }

    function checkCredentials() {
        if(window.localStorage.getItem('pnd_token')){
            $rootScope.roles = JSON.parse(window.localStorage.getItem('roles'));
            $rootScope.token = window.localStorage.getItem('pnd_token');
            mainVm.roles = $rootScope.roles;
        } else {
            $state.go('login');
        }
    }

}
/**
 *
 * Pass all functions into module
 */
angular
    .module('Flyrobe')
    .controller('MainCtrl', MainCtrl);
