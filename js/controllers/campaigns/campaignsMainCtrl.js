/*controller for displaying saved pushNotification template */

function campaignsMainCtrl($state, $timeout,$uibModal, $localStorage, $http, $scope, $rootScope, $log, mainServices) {
	$scope.p1 = $rootScope.roles[0].panels.CAMP.P1
	$scope.p2 = $rootScope.roles[0].panels.CAMP.P2
	$scope.test_input = ""

	campaignVm = this;
	campaignVm.test_input = $scope.test_input
	mainVm.checkCredentials();
	$localStorage.pn_state = "Campaign";
	mainVm.pn_state = $localStorage.pn_state;
   
	$scope.loading = true;
	$scope.open2 = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/modalWindowCampaigns.html',
			controller: ModalInstanceCtrl
		});
	};
	
	/*$scope.searchThrough = (e) => {
		if(campaignVm.test_input != ""){
			$scope.data = $scope.data_to_filter.filter(p => {
				return p.campaign_name.includes(campaignVm.test_input)
			})
		}
	}*/

	/**Function to get list of campaign template*/
		
	$http({
		method: 'GET',
		url: mainVm.urlList.node_url2 + 'get_campaigns/?page=1&limit=30', // No need of IP address
		headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
	}).then(function successCallback(response) {
		$scope.loading = false;
		$scope.data_to_filter = response.data.data;
		$scope.data = response.data.data;
		$timeout(function () {
			$('.table').trigger('footable_redraw');
		}, 100);

	}, function errorCallback(response) {
		mainServices.checkErrorStatus(response.status);
	});
};

angular
    .module('Flyrobe')
	.controller('campaignsMainCtrl', campaignsMainCtrl);
