/*controller to create campaigns */

function createCampaignsCtrl($state, $timeout, $uibModal, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices,
							 commonService) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "Create Campaign";
	mainVm.pn_state = $localStorage.pn_state;

	ccCtrl = this;
	ccCtrl.spinner = false;

	$scope.segmentName = [];
	$scope.pnTemplateName = [];
	$scope.smsTemplateName = [];
	$scope.emailTemplateName = [];
	$scope.send_channels = {'sms' : {}, 'push' : {}, 'email' : {}};
	$scope.push_notification = '';
	ccCtrl.stringModel = [];
	$scope.stringData = ['David', 'Jhon', 'Danny'];
	$scope.stringSettings = { template: '{{option}}', smartButtonTextConverter(skip, option) { return option; } };
	$scope.example10settings = { selectionLimit: 2 };

	$rootScope.datepickerOptionsStartDate = {
		minDate: new Date()
	}

	ccCtrl.setTemplateProperties  		= setTemplateProperties;
	ccCtrl.createCampaign  				= createCampaign;

	ccCtrl.setTemplateProperties();//default Call




	function setTemplateProperties() {

		if($state.current.name.indexOf('oneTimeCampaigns') >= 0){
			$scope.campaignType = "one_time_triggered";
			ccCtrl.template_name = "One Time Triggered Campaign";
		}
		else if($state.current.name.indexOf('eventCampaigns') >= 0){
			$scope.campaignType = "event_triggered";
			ccCtrl.template_name = "Event Triggered Campaign";
		}
		else if($state.current.name.indexOf('segmentCampaigns') >= 0){
			$scope.campaignType = "segment_triggered";
			ccCtrl.template_name = "Segment Triggered Campaign";
		}

	}

	function createCampaign() {

		if ($scope.campaignName == "" || $scope.campaignName == null || $scope.campaignName == undefined ||
			$scope.campaignType == "" || $scope.campaignType == null || $scope.campaignName == undefined ||
			$scope.startDate == "" || $scope.startDate == null || $scope.startDate == undefined ||
			$scope.startHours === "" || $scope.startHours == null || $scope.startHours == undefined || $scope.startHours < 0 ||
			$scope.startMinutes === "" || $scope.startMinutes == null || $scope.startMinutes == undefined || $scope.startMinutes < 0 ||
			$scope.startMed == "" || $scope.startMed == null || $scope.startMed == undefined ||
			$scope.selectSegmentEvent == "" || $scope.selectSegmentEvent == null || $scope.selectSegmentEvent == undefined) {
			alert("Please enter all required fields.");
			return false;
		}


		if($state.current.name.indexOf('oneTimeCampaigns') < 0){
			if ($scope.endDate === "" || $scope.endDate == null || $scope.endDate == undefined ||
				$scope.endHour === "" || $scope.endHour == null || $scope.endHour == undefined || $scope.endHour < 0 ||
				$scope.endMinute === "" || $scope.endMinute == null || $scope.endMinute == undefined || $scope.endMinute < 0 ||
				$scope.endMed == "" || $scope.endMed == null || $scope.endMed == undefined) {
				alert("Please enter all required fields.");
				return false;
			}

			if ($scope.endHour > 12 || $scope.startHours > 12 || $scope.startMinutes > 60 || $scope.endMinute > 60) {
				alert("Please enter valid date & time.");
				return false;
			}

			var start = $filter('date')(new Date($scope.startDate),'yyyy-MM-dd') + " " + $scope.startHours + ":" + $scope.startMinutes + " " + $scope.startMed;
			var end = $filter('date')(new Date($scope.endDate),'yyyy-MM-dd') + " " + $scope.endHour + ":" + $scope.endMinute + " " + $scope.endMed;

			if(commonService.timeBetween(new Date(),new Date(start)) <= 0){
				alert("Please enter valid date & time.");
				return false;
			}

			if(commonService.timeBetween(new Date(start),new Date(end)) <= 0){
				alert("Please enter valid date & time.");
				return false;
			}


		}
		else{

			if ($scope.startHours > 12 || $scope.startMinutes > 60) {
				alert("Please enter valid date & time.");
				return false;
			}

			var start = $filter('date')(new Date($scope.startDate),'yyyy-MM-dd') + " " + $scope.startHours + ":" + $scope.startMinutes + " " + $scope.startMed;

			if(commonService.timeBetween(new Date(),new Date(start)) <= 0){
				alert("Please enter valid date & time.");
				return false;
			}

		}

		if($state.current.name.indexOf('eventCampaigns') >= 0){
			if(ccCtrl.stringModel == "" || ccCtrl.stringModel == null || ccCtrl.stringModel == undefined || ccCtrl.stringModel.length == 0){
				alert("Please enter all required fields.");
				return false;
			}
		}

		var channels = [];

		if($scope.send_channels['push'].name){
			channels.push($scope.send_channels['push']);
		}

		if($scope.send_channels['sms'].name){
			channels.push($scope.send_channels['sms']);
		}

		if($scope.send_channels['email'].name){
			channels.push($scope.send_channels['email']);
		}

		if(channels.length == 0){
			alert("Please add atleast one channel.");
			return false;
		}


		var send_data = {
			"campaign_name": $scope.campaignName,
			"campaign_type": $scope.campaignType,
			"start_date": {
				"date": $filter('date')($scope.startDate, "yyyy-MM-dd", "UTC"),
				"time": $scope.startHours + ":" + $scope.startMinutes,
				"meridiem": $scope.startMed
			},
			"end_date": {
				"date": $filter('date')($scope.endDate, "yyyy-MM-dd", "UTC"),
				"time": $scope.endHour + ":" + $scope.endMinute,
				"meridiem": $scope.endMed
			},
			"segment_or_event_name": $scope.selectSegmentEvent,
			"deactivate_event": ccCtrl.stringModel,
			"repetition_type": ($scope.repetition_type == 'recurring' ? 'recurring' : 'non_recurring'),
			"channel" : channels
		};

		ccCtrl.spinner = true;

		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'create_campaign/',
			data: send_data,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {

			ccCtrl.spinner = false;

			if (response.data.is_error == true) {
				alert(response.data.message);
			}

			else if (response.data.status == 200) {
				// $scope.open1();

				alert("Successfully completed.");
				$state.go('home.campaigns');
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

	}


	$scope.openPushPreview = function () {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/pushPreviewModal.html',
			controller: ModalInstanceCtrl
		});
	};
	$scope.openSmsPreview = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/smsPreviewModal.html',
			controller: ModalInstanceCtrl
		});
	};
	$scope.openEmailPreview = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/emailPreviewModal.html',
			controller: ModalInstanceCtrl
		});
	};

	/**Function to get list of segment */
	$scope.getSegment = function () {

		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_segments/', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_1 }
		}).then(function successCallback(response) {
			$scope.segmentArr = response.data.data;

			for (var i = 0; i < $scope.segmentArr.length; i++) {
				$scope.segmentName.push($scope.segmentArr[i].segment_name)
			}

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};

	$scope.getSegment();//Default Call

	/**Function to get list of pn template*/
	$scope.getPnTemplate = function () {
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_pn_templates/?page=1&limit=30', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;

			$scope.pnTemplateArr = response.data.data;

			for (var i = 0; i < $scope.pnTemplateArr.length; i++) {
				$scope.pnTemplateName.push($scope.pnTemplateArr[i].template_name);
			}

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};

	$scope.getPnTemplate();//Default Call

	/**Function to get list of sms template*/
	$scope.getSmsTemplate = function () {
		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_sms_templates/', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;

			$scope.smsTemplateArr = response.data.data;

			for (var i = 0; i < $scope.smsTemplateArr.length; i++) {
				$scope.smsTemplateName.push($scope.smsTemplateArr[i].template_name);
			}

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};

	$scope.getSmsTemplate();//Default Call

	/**Function to get list of email template*/
	$scope.getEmailTemplate = function () {
		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_email_templates/', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;

			$scope.emailTemplateArr = response.data.data;

			for (var i = 0; i < $scope.emailTemplateArr.length; i++) {
				$scope.emailTemplateName.push($scope.emailTemplateArr[i].template_name);
			}

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};

	$scope.getEmailTemplate();//Default Call

	/**Function to get list of event*/
	$scope.getEvent = function () {

		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_event_list/Android', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.eventName = response.data.data;

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

	};

	$scope.getEvent();//Default Call

	$scope.push_channel = [];
	$scope.sms_channel = [];
	$scope.email_channel = [];

	/**Function to set channel name */
	$scope.pushChannel = function () {

		$scope.channel_name = "push_notification";

		if($scope.push_channel.length == 0 || commonService.emptyObject($scope.push_channel[0])){
			$scope.push_channel = [{
				value_push : 0
			}];
			$scope.push=true;
			$scope.div=false;
		}
		else{
			alert('Channel is already created');
		}

	};

	$scope.smsChannel = function () {
		$scope.channel_name = "sms";

		if($scope.sms_channel.length == 0 || commonService.emptyObject($scope.sms_channel[0])){
			$scope.sms_channel = [{
				value_push : 0
			}];
			$scope.sms=true;
			$scope.div=false;
		}
		else{
			alert('Channel is already created');
		}

	}

	$scope.emailChannel = function () {
		$scope.channel_name = "email";

		if($scope.email_channel.length == 0 || commonService.emptyObject($scope.email_channel[0])){
			$scope.email_channel = [{
				value_push : 0
			}];
			$scope.email=true;
			$scope.div=false;
		}
		else{
			alert('Channel is already created');
		}
	}

	/**Function to set campaign type */
	$scope.setSegmentType = function () {
		$scope.campaignType = "segment_triggered";
	}
	$scope.setEventType = function () {
		$scope.campaignType = "event_triggered";
	}
	$scope.setOneTimeType = function () {
		$scope.campaignType = "one_time_triggered";
	}

	/**Function to get push preview */
	$scope.getPushPreivew =function() {


		if($scope.push_channel[0] == "" || $scope.push_channel[0] == null || $scope.push_channel[0] == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($state.current.name.indexOf('eventCampaigns') >= 0){
			if($scope.push_channel[0].value_push === "" || $scope.push_channel[0].value_push == null || $scope.push_channel[0].value_push == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.push_channel[0].unit_push === "" || $scope.push_channel[0].unit_push == null || $scope.push_channel[0].unit_push == undefined){
				alert("Please enter all required fields.");
				return false;
			}
		}



		/*if($scope.push_channel[0].when_push == "" || $scope.push_channel[0].when_push == null || $scope.push_channel[0].when_push == undefined){
			alert("Please enter all required fields.");
			return false;
		}*/

		if($scope.push_channel[0].selectPnTemplate == "" || $scope.push_channel[0].selectPnTemplate == null || $scope.push_channel[0].selectPnTemplate == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($scope.repetition_type == 'recurring'){

			if($scope.push_channel[0].frequency == "" || $scope.push_channel[0].frequency == null || $scope.push_channel[0].frequency == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.push_channel[0].frequency_time == "" || $scope.push_channel[0].frequency_time == null || $scope.push_channel[0].frequency_time == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.push_channel[0].frequency_unit == "" || $scope.push_channel[0].frequency_unit == null || $scope.push_channel[0].frequency_unit == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($state.current.name.indexOf('eventCampaigns') >= 0){
				$scope.send_channels['push'] = {
					"name": 'push_notification',
					"selected_template": $scope.push_channel[0].selectPnTemplate,
					"frequency_obj": {
						"frequency": $scope.push_channel[0].frequency,
						"frequency_time": $scope.push_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.push_channel[0].frequency_unit
					},
					"waiting_time": {
						"value": parseInt($scope.push_channel[0].value_push),
						"unit": $scope.push_channel[0].unit_push,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['push'] = {
					"name": 'push_notification',
					"selected_template": $scope.push_channel[0].selectPnTemplate,
					"frequency_obj": {
						"frequency": $scope.push_channel[0].frequency,
						"frequency_time": $scope.push_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.push_channel[0].frequency_unit
					}
				};
			}

		}
		else {


			if($state.current.name.indexOf('eventCampaigns') >= 0){
				$scope.send_channels['push'] = {
					"name": 'push_notification',
					"selected_template": $scope.push_channel[0].selectPnTemplate,
					"waiting_time": {
						"value": parseInt($scope.push_channel[0].value_push),
						"unit": $scope.push_channel[0].unit_push,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['push'] = {
					"name": 'push_notification',
					"selected_template": $scope.push_channel[0].selectPnTemplate
				};
			}


		}

		$scope.push=false;
		$scope.div=true;
		$scope.push_preview=true;



		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_pn_templates/?page=1&limit=30', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;
	
			$scope.segmentArr = response.data.data;
			
            for (var i = 0; i < $scope.segmentArr.length; i++) {
                if (($scope.segmentArr[i].template_name) === ($scope.push_channel[0].selectPnTemplate)) {
					$rootScope.title_push= $scope.segmentArr[i].template_content.content.title;
					$rootScope.body_push= $scope.segmentArr[i].template_content.content.body;
					$rootScope.big_image_push= $scope.segmentArr[i].template_content.content.big_image;
					$rootScope.large_icon_push= $scope.segmentArr[i].template_content.content.large_icon;
					$rootScope.template_type_push= $scope.segmentArr[i].template_content.template_type;
					
                }
            }
	
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	}

	/**Function to get sms preview */
	$scope.getSmsPreivew =function() {


		if($scope.sms_channel[0] == "" || $scope.sms_channel[0] == null || $scope.sms_channel[0] == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($state.current.name.indexOf('eventCampaigns') >= 0){
			if($scope.sms_channel[0].value_sms === "" || $scope.sms_channel[0].value_sms == null || $scope.sms_channel[0].value_sms == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.sms_channel[0].unit_sms == "" || $scope.sms_channel[0].unit_sms == null || $scope.sms_channel[0].unit_sms == undefined){
				alert("Please enter all required fields.");
				return false;
			}
		}


		/*if($scope.sms_channel[0].when_sms == "" || $scope.sms_channel[0].when_sms == null || $scope.sms_channel[0].when_sms == undefined){
			alert("Please enter all required fields.");
			return false;
		}*/

		if($scope.sms_channel[0].selectSmsTemplate == "" || $scope.sms_channel[0].selectSmsTemplate == null || $scope.sms_channel[0].selectSmsTemplate == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($scope.repetition_type == 'recurring'){

			if($scope.sms_channel[0].frequency == "" || $scope.sms_channel[0].frequency == null || $scope.sms_channel[0].frequency == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.sms_channel[0].frequency_time == "" || $scope.sms_channel[0].frequency_time == null || $scope.sms_channel[0].frequency_time == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.sms_channel[0].frequency_unit == "" || $scope.sms_channel[0].frequency_unit == null || $scope.sms_channel[0].frequency_unit == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($state.current.name.indexOf('eventCampaigns') >= 0){
				$scope.send_channels['sms'] = {
					"name": 'sms',
					"selected_template": $scope.sms_channel[0].selectSmsTemplate,
					"frequency_obj": {
						"frequency": $scope.sms_channel[0].frequency,
						"frequency_time": $scope.sms_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.sms_channel[0].frequency_unit
					},
					"waiting_time": {
						"value": parseInt($scope.sms_channel[0].value_sms),
						"unit": $scope.sms_channel[0].unit_sms,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['sms'] = {
					"name": 'sms',
					"selected_template": $scope.sms_channel[0].selectSmsTemplate,
					"frequency_obj": {
						"frequency": $scope.sms_channel[0].frequency,
						"frequency_time": $scope.sms_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.sms_channel[0].frequency_unit
					}
				};
			}



		}
		else {
			if($state.current.name.indexOf('eventCampaigns') >= 0) {
				$scope.send_channels['sms'] = {
					"name": 'sms',
					"selected_template": $scope.sms_channel[0].selectSmsTemplate,
					"waiting_time": {
						"value": parseInt($scope.sms_channel[0].value_sms),
						"unit": $scope.sms_channel[0].unit_sms,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['sms'] = {
					"name": 'sms',
					"selected_template": $scope.sms_channel[0].selectSmsTemplate
				};
			}
		}

		$scope.sms=false;
		$scope.div=true;
		$scope.sms_preview=true;
		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_sms_templates/', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;

			$scope.smsArr = response.data.data;
			
            for (var i = 0; i < $scope.smsArr.length; i++) {

                if (($scope.smsArr[i].template_name) == ($scope.sms_channel[0].selectSmsTemplate)) {
					$rootScope.title_sms= $scope.smsArr[i].template_name;
					$rootScope.content_sms= $scope.smsArr[i].template_content;
					
                }
            }
	
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	}

	/**Function to get email preview */
	$scope.getEmailPreivew =function() {

		if($scope.email_channel[0] == "" || $scope.email_channel[0] == null || $scope.email_channel[0] == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($state.current.name.indexOf('eventCampaigns') >= 0) {
			if($scope.email_channel[0].value_email === "" || $scope.email_channel[0].value_email == null || $scope.email_channel[0].value_email == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.email_channel[0].unit_email == "" || $scope.email_channel[0].unit_email == null || $scope.email_channel[0].unit_email == undefined){
				alert("Please enter all required fields.");
				return false;
			}
		}



		/*if($scope.email_channel[0].when_email == "" || $scope.email_channel[0].when_email == null || $scope.email_channel[0].when_email == undefined){
			alert("Please enter all required fields.");
			return false;
		}*/

		if($scope.email_channel[0].selectEmailTemplate == "" || $scope.email_channel[0].selectEmailTemplate == null || $scope.email_channel[0].selectEmailTemplate == undefined){
			alert("Please enter all required fields.");
			return false;
		}

		if($scope.repetition_type == 'recurring'){

			if($scope.email_channel[0].frequency == "" || $scope.email_channel[0].frequency == null || $scope.email_channel[0].frequency == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.email_channel[0].frequency_time == "" || $scope.email_channel[0].frequency_time == null || $scope.email_channel[0].frequency_time == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($scope.email_channel[0].frequency_unit == "" || $scope.email_channel[0].frequency_unit == null || $scope.email_channel[0].frequency_unit == undefined){
				alert("Please enter all required fields.");
				return false;
			}

			if($state.current.name.indexOf('eventCampaigns') >= 0) {
				$scope.send_channels['email'] = {
					"name": 'email',
					"selected_template": $scope.email_channel[0].selectEmailTemplate,
					"frequency_obj": {
						"frequency": $scope.email_channel[0].frequency,
						"frequency_time": $scope.email_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.email_channel[0].frequency_unit
					},
					"waiting_time": {
						"value": parseInt($scope.email_channel[0].value_email),
						"unit": $scope.email_channel[0].unit_email,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['email'] = {
					"name": 'email',
					"selected_template": $scope.email_channel[0].selectEmailTemplate,
					"frequency_obj": {
						"frequency": $scope.email_channel[0].frequency,
						"frequency_time": $scope.email_channel[0].frequency_time,
						"frequency_count": 0,
						"frequency_unit": $scope.email_channel[0].frequency_unit
					}
				};
			}

		}
		else {
			if($state.current.name.indexOf('eventCampaigns') >= 0) {
				$scope.send_channels['email'] = {
					"name": 'email',
					"selected_template": $scope.email_channel[0].selectEmailTemplate,
					"waiting_time": {
						"value": parseInt($scope.email_channel[0].value_email),
						"unit": $scope.email_channel[0].unit_email,
						"when": 'triggered_event'
					}
				};
			}
			else{
				$scope.send_channels['email'] = {
					"name": 'email',
					"selected_template": $scope.email_channel[0].selectEmailTemplate
				};
			}
		}


		$scope.email=false;
		$scope.div=true;
		$scope.email_preview=true;
		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_email_templates/', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;
	
			$scope.emailArr = response.data.data;
			
            for (var i = 0; i < $scope.emailArr.length; i++) {
                if (($scope.emailArr[i].template_name) === ($scope.email_channel[0].selectEmailTemplate)) {
					$rootScope.title_email= $scope.emailArr[i].template_name;
					$rootScope.content_email= $scope.emailArr[i].template_content;
					
                }
            }
	
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	}

	/**Function for creating campaigns*/
	/*$scope.createCampaign = function () {

		if ($scope.campaignName == "" || $scope.campaignName == null || $scope.campaignName == undefined ||
			$scope.campaignType == "" || $scope.campaignType == null || $scope.campaignName == undefined ||
			$scope.startHours == "" || $scope.startHours == null || $scope.startHours == undefined ||
			$scope.startMinutes == "" || $scope.startMinutes == null || $scope.startMinutes == undefined ||
			$scope.startMed == "" || $scope.startMed == null || $scope.startMed == undefined ||
			$scope.selectSegmentEvent == "" || $scope.selectSegmentEvent == null || $scope.selectSegmentEvent == undefined) {
			alert("Please enter all required fields.");
			return false;
		}

		if($state.current.name.indexOf('oneTimeCampaigns') < 0){
			if ($scope.endHour == "" || $scope.endHour == null || $scope.endHour == undefined ||
				$scope.endMinute == "" || $scope.endMinute == null || $scope.endMinute == undefined ||
				$scope.endMed == "" || $scope.endMed == null || $scope.endMed == undefined) {
				alert("Please enter all required fields.");
				return false;
			}
		}

		if($state.current.name.indexOf('eventCampaigns') >= 0){
			if(ccCtrl.stringModel == "" || ccCtrl.stringModel == null || ccCtrl.stringModel == undefined || ccCtrl.stringModel.length == 0){
				alert("Please enter all required fields.");
				return false;
			}
		}



		var channels = [];

		if($scope.send_channels['push'].name){
			channels.push($scope.send_channels['push']);
		}

		if($scope.send_channels['sms'].name){
			channels.push($scope.send_channels['sms']);
		}

		if($scope.send_channels['email'].name){
			channels.push($scope.send_channels['email']);
		}

		if(channels.length == 0){
			alert("Please add atleast one channel.");
			return false;
		}

		var send_data = {
			"campaign_name": $scope.campaignName,
			"campaign_type": $scope.campaignType,
			"start_date": {
				"date": $filter('date')($scope.startDate, "yyyy-MM-dd", "UTC"),
				"time": $scope.startHours + ":" + $scope.startMinutes,
				"meridiem": $scope.startMed
			},
			"end_date": {
				"date": $filter('date')($scope.endDate, "yyyy-MM-dd", "UTC"),
				"time": $scope.endHour + ":" + $scope.endMinute,
				"meridiem": $scope.endMed
			},
			"segment_or_event_name": $scope.selectSegmentEvent,
			"deactivate_event": ccCtrl.stringModel,
			"repetition_type": ($scope.repetition_type == 'recurring' ? 'recurring' : 'non_recurring'),
			"channel" : channels
		};

		ccCtrl.spinner = true;

		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'create_campaign/',
			data: send_data
			/!*{
				"campaign_name": $scope.campaignName,
				"campaign_type": $scope.campaignType,
				"start_date": {
					"date": $filter('date')($scope.startDate, "yyyy-MM-dd", "UTC"),
					"time": $scope.startHours + ":" + $scope.startMinutes,
					"meridiem": $scope.startMed
				},
				"end_date": {
					"date": $filter('date')($scope.endDate, "yyyy-MM-dd", "UTC"),
					"time": $scope.endHour + ":" + $scope.endMinute,
					"meridiem": $scope.endMed
				},
				"segment_or_event_name": $scope.selectSegmentEvent,
				"deactivate_event": ccCtrl.stringModel,
				"repetition_type": ($scope.repetition_type == 'recurring' ? 'recurring' : 'non_recurring'),
				"channel" : channels
				/!*"channel": [

					{
						"name": $scope.channel_name,
						"selected_template": $scope.selectPnTemplate,
						"frequency_obj": {
							"frequency": $scope.frequency,
							"frequency_time": $scope.frequency_time,
							"frequency_count": 0,
							"frequency_unit": $scope.frequency_unit
						},
						"waiting_time": {
							"value": parseInt($scope.value_push),
							"unit": $scope.unit_push,
							"when": $scope.when_push
						}
					},
					{
						"name": $scope.channel_name,
						"selected_template": "test_email_template",
						"frequency_obj": {
							"frequency": $scope.frequency,
							"frequency_time": $scope.frequency_time,
							"frequency_count": 0,
							"frequency_unit": $scope.frequency_unit
						},
						"waiting_time": {
							"value": parseInt($scope.value_email),
							"unit": $scope.unit_email,
							"when": $scope.when_email

						}
					},
					{
						"name": $scope.channel_name,
						"selected_template": "test_sms_template",
						"frequency_obj": {
							"frequency": $scope.frequency,
							"frequency_time": $scope.frequency_time,
							"frequency_count": 0,
							"frequency_unit": $scope.frequency_unit
						},
						"waiting_time": {
							"value": parseInt($scope.value_sms),
							"unit": $scope.unit_sms,
							"when": $scope.when_sms

						}
					}

				]*!/

			}*!/
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {

			ccCtrl.spinner = false;


			if (response.data.is_error == true) {

				alert("Oops :: " , response.data.message);
			}

			else if (response.data.status == 200) {
				// $scope.open1();

				alert("Successfully completed.");
				$state.go('home.campaigns');
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			ccCtrl.spinner = false;
			mainServices.checkErrorStatus(response.status);
		});


	};*/

};

angular
	.module('Flyrobe')
	.controller('createCampaignsCtrl', createCampaignsCtrl);
