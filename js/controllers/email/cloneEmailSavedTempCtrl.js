function cloneEmailSavedTempCtrl($state, $timeout,$window, $uibModal, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices, codeService
,$stateParams) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "Email";
	mainVm.pn_state = $localStorage.pn_state;

	$scope.spinner = true;

    $scope.editorOptions1 = {mode: 'text/html',
    lineNumbers: true,
    matchBrackets: true};

    // $scope.html =$rootScope.selectedEmail.template_content;

    $scope.$watch('html', function () { codeService.render($scope.html, $scope.css); }, true);
    $scope.$watch('css', function () { codeService.render($scope.html, $scope.css); }, true);

    /**Function for making clone of template*/
	$scope.saveTemp = function () {
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'save_email_templates/',
			data: {
                "template_name": $rootScope.cloneEmailTemplateName,
				"template_content": $scope.html,
				"subject":$rootScope.selectedEmail.subject

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {


			if (response.data.is_error == true) {

                swal("Oops :: " + response.data.message);
                console.log(response.data.message);
			}

			else if (response.data.status == 200) {
				// $scope.open1();

				swal("Successfully completed :: ");
				$state.go('home.email')
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');

	};


	$scope.getTemplate = function() {

		var name = $stateParams.template_name;

		var tname = name.replace(/%20/g, " ");
		console.log(tname);


		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_email_templates/' + tname,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).success(function(response) {
			$scope.spinner = false;

			$rootScope.cloneEmailTemplateName = "Clone_Of_" + response.data[0].template_name;
				$scope.html = response.data[0].template_content;
				$rootScope.selectedEmail.subject = response.data[0].subject;
		});

	}

	$scope.getTemplate();//Default Call

};

angular
	.module('Flyrobe')
	.controller('cloneEmailSavedTempCtrl', cloneEmailSavedTempCtrl);
