function createEmailCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $filter, $log,
                         $uibModal, mainServices, codeService) {

    mainVm.checkCredentials();

    $localStorage.pn_state = "Create Email";
    mainVm.pn_state = $localStorage.pn_state;

    $scope.editorOptions1 = {mode: 'text/html',
        lineNumbers: true,
        matchBrackets: true};

    $scope.html = '<body style="margin-top:100px">default</body>';
    $scope.css = "body {color: red}";

    $scope.$watch('html', function () { codeService.render($scope.html, $scope.css); }, true);
    $scope.$watch('css', function () { codeService.render($scope.html, $scope.css); }, true);


    $scope.open = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/pages/email/saveModalWindow.html',
            controller: ModalInstanceCtrl
        });
    };
    $scope.open1 = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/pages/email/sendModalWindow.html',
            controller: ModalInstanceCtrl
        });
    };

    /**Function to validate form */
    $scope.validateHtml = function () {
        $rootScope.email_html = $scope.html;
    }

};

angular
    .module('Flyrobe')
    .controller('createEmailCtrl', createEmailCtrl);


