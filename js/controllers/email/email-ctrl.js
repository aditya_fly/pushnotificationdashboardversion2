/*controller to create campaigns */

function emailMainCtrl($state, $timeout, $uibModal, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices) {

    emailVm = this;
    emailVm.spinner = true;

    mainVm.checkCredentials();
    $localStorage.pn_state = "Email";
    mainVm.pn_state = $localStorage.pn_state;

    emailVm.getEmailTemplateList    = getEmailTemplateList;

    emailVm.getEmailTemplateList();//Default Call


    $scope.p1 = mainVm.roles[0].panels.EMAIL.P1
    $scope.p2 = mainVm.roles[0].panels.EMAIL.P2

    // emailVm.test_input = "";

    /*$scope.searchThrough = () => {
        if (emailVm.test_input != "") {
            $scope.data = $scope.filter_data.filter(p => {
                    return p.template_name.includes(emailVm.test_input)
                })
        }
    }*/

    /**Function to get list of sms template*/
    function getEmailTemplateList() {
        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_email_templates/?page=1&limit=15', // No need of IP address
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {
            emailVm.spinner = false;
            $scope.filter_data = response.data.data;
            $scope.data = response.data.data;
            $timeout(function () {
                $('.table').trigger('footable_redraw');
            }, 100);


        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });
    }




    $scope.selectedEmailTemplate = function (Email) {
        $rootScope.selectedEmail = Email;
        $rootScope.cloneEmailTemplateName = "Clone of " + Email.template_name;
    }



};

angular
    .module('Flyrobe')
    .controller('emailMainCtrl', emailMainCtrl);
