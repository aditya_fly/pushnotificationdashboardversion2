function saveModalWindowCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $filter, $log,$uibModal, mainServices, codeService) {

	mainVm.checkCredentials();

	/**Function for saving template*/
	$scope.saveTemp = function () {
        
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'save_email_templates/',
			data: {
                "template_name": $scope.template_Name,
				"template_content": $rootScope.email_html,
				"subject":$scope.subject

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {


			if (response.data.is_error == true) {

                swal("Oops :: " + response.data.message);
                console.log(response.data.message);
			}

			else if (response.data.status == 200) {
				// $scope.open1();

				swal("Successfully completed :: ");
				$state.go('home.email')
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');

	};

};

angular
	.module('Flyrobe')
    .controller('saveModalWindowCtrl', saveModalWindowCtrl);
    
    
