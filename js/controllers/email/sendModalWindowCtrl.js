function sendModalWindowCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $filter, $log, $uibModal,
    mainServices, codeService) {

    sendEmailVm = this;

    mainVm.checkCredentials();

    $scope.userArr = [
        // { id: 1, user: 'All Users' },
        { id: 2, user: 'Selected Users' },
        { id: 3, user: 'Segments' }
    ];

    $scope.segmentName = [];
    $scope.emailList = [];
    sendEmailVm.email_field = '';

    /*Function to get email id of all users and list of all segment*/
    $scope.check = function () {
        if ($scope.userData.userSelected.id == 1) {
            $http({
                method: 'GET',
                url: mainVm.urlList.node_url2 + 'all_user_email', // No need of IP address
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': mainVm.AuthorizationHeaderList.Authorization_2
                }
            }).then(function successCallback(response) {
                $scope.ss = response.data.data;

            }, function errorCallback(response) {
                mainServices.checkErrorStatus(response.status);
            });

        }
        else if ($scope.userData.userSelected.id == 3) {
            $http({
                method: 'GET',
                url: mainVm.urlList.node_url2 + 'get_segments/', // No need of IP address
                headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_1 }
            }).then(function successCallback(response) {
                $scope.segmentArr = response.data.data;
                for (var i = 0; i < $scope.segmentArr.length; i++) {
                    $scope.segmentName.push($scope.segmentArr[i].segment_name)
                }

            }, function errorCallback(response) {
                mainServices.checkErrorStatus(response.status);
            });

        }
        else {

        }
    };

    /*Function to get email id of particular segemnt */
    $scope.checkEvent = function () {
        $scope.Android = $scope.userData.eventArr;

        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_segments/', // No need of IP address
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_1 }
        }).then(function successCallback(response) {
            $scope.segmentArr = response.data.data;
            for (var i = 0; i < $scope.segmentArr.length; i++) {
                if (($scope.segmentArr[i].segment_name) === ($scope.Android)) {
                    $scope.emailList = $scope.segmentArr[i].segment_email;
                }
            }
            $scope.ss = $scope.emailList;
            alert($scope.ss);
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });


    };

    /**Function for saving template*/
    $scope.saveTemp = function () {

        $http({
            method: 'POST',
            url: mainVm.urlList.node_url2 + 'save_email_templates/',
            data: {
                "template_name": $scope.template_Name,
                "template_content": $rootScope.email_html,
                "subject": $scope.subject

            }
            ,
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {


            if (response.data.is_error == true) {
                swal("Oops :: " + response.data.message);
            }
            else if (response.data.status == 200) {
                $scope.sendTemp();
            }

            $scope.loading = false;

        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });

    };


    /**Function for sending email template */
    $scope.sendTemp = function () {

        // if ($scope.userData.userSelected.id == 1) {

        //     $scope.s = $scope.email_field;
        //     $scope.ss = $scope.s.split(",");
        // }
        console.log(sendEmailVm.email_field)


        $http({
            method: 'POST',
            url: mainVm.urlList.node_url2 + 'send_email/',
            data: {
                "to_emails": sendEmailVm.email_field.split(','),
                "text_or_email": $rootScope.email_html,
                "subject": $scope.subject

            },
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {


            if (response.data.is_error == true) {
                alert("Oops :: " + response.data.message);
            }

            else if (response.data.status == 200) {
                $rootScope.ok();
                swal("Successfully sent");
                $state.go('home.email');

            }
            $scope.loading = false;
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });

        this.userData = {};
    };

};

angular
    .module('Flyrobe')
    .controller('sendModalWindowCtrl', sendModalWindowCtrl);


