function updateSavedEmailTemplateCtrl($state, $timeout,$window, $uibModal, $localStorage, $http, $scope, $rootScope,
									  $filter, $log, mainServices, codeService,$stateParams) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "Email";
	mainVm.pn_state = $localStorage.pn_state;

    $scope.editorOptions1 = {mode: 'text/html',
    lineNumbers: true,
    matchBrackets: true};

	$scope.spinner = true;

	$rootScope.selectedEmail = {};

    // $scope.html =$rootScope.selectedEmail.template_content;

    $scope.$watch('html', function () { codeService.render($scope.html, $scope.css); }, true);
    $scope.$watch('css', function () { codeService.render($scope.html, $scope.css); }, true);

    /**Function to update email template */
    $scope.updateEmail = function () {
		$http({
			method: 'PUT',
			url: mainVm.urlList.node_url2 + 'update_email_template/' + $scope.selectedEmail.template_name,
			data: {
            
	            "template_content":  $scope.html

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {


			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			} else {
				var modalInstance = $uibModal.open({
					template: '<h1>Succesfully updated</h1>',
				});
				$state.go('home.email')
			}

			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');

	};

	$scope.open1 = function () {
		$rootScope.template_name = $scope.selectedEmail.template_name;
		$rootScope.email_html = $scope.html;
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/email/send-update-email-modal.html',
			controller: ModalInstanceCtrl
		});
	};


	$scope.getTemplate = function() {

		var name = $stateParams.template_name;

		var tname = name.replace(/%20/g, " ");
		console.log(tname);


		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_email_templates/' + tname,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).success(function(response) {
			$scope.spinner = false;

			$scope.selectedEmail.template_name = response.data[0].template_name;
			$scope.html = response.data[0].template_content;
			$rootScope.selectedEmail.subject = response.data[0].subject;
		});

	}

	$scope.getTemplate();//Default Call
	
	/**Function to validate form */
	/*$scope.validateHtml = function () {
		$rootScope.email_html = $scope.html;
	}*/

};

angular
	.module('Flyrobe')
	.controller('updateSavedEmailTemplateCtrl', updateSavedEmailTemplateCtrl);
