/*contoller for tracking liveEvent */
function liveEventsCtrl($state, $timeout, $localStorage, $http, $scope, $log, mainServices) {


      le = this;
      mainVm.checkCredentials();
      $localStorage.pn_state = "Live Events";
      mainVm.pn_state = $localStorage.pn_state;

      $scope.CurrentDate = new Date();
      $scope.loading = true;

      $scope.users = [];
      $scope.data = [];

      le.getEvents            = getEvents; //function to get event list

      le.getEvents(); //Default Call

      function getEvents() {

            $http({
                  method: 'GET',
                  url: mainVm.urlList.node_url2 + 'get_event/?platform=Android&page=1&limit=15',
                  headers: {
                        'Content-Type': 'application/json',
                        'Authorization': mainVm.AuthorizationHeaderList.Authorization_1
                  }
            }).then(function successCallback(response) {
                  $scope.loading = false;
                  $scope.data = response.data.data;
                  $scope.event_count = response.data.data.length;

                  $scope.data.forEach(function (col) {

                        col.test = [
                              {property_name : 'User ID',value : col.user_properties.Social_id},
                              {property_name : 'Name',value : col.user_properties.Name},
                              {property_name : 'Gender',value : col.user_properties.Gender},
                              {property_name : 'First Name',value : col.user_properties.First_name},
                              {property_name : 'Last Name',value : col.user_properties.Last_name},
                              {property_name : 'City',value : col.user_properties.City},
                              {property_name : 'Email',value : col.user_properties.Email},
                              {property_name : 'Mobile',value : col.user_properties.Mobile},
                              {property_name : 'Channel',value : col.user_properties.Channel},
                              {property_name : 'Flyrobe Analytic version',value : col.platform_properties._flyrobe_analytics_version},
                              {property_name : 'Android OS name',value : col.platform_properties._android_os_name},
                              {property_name : 'Android OS version',value : col.platform_properties._android_os_version},
                              {property_name : 'Android Manufacturer',value : col.platform_properties._android_manufacturer},
                              {property_name : 'Android Brand',value : col.platform_properties._android_brand},
                              {property_name : 'Android Model',value : col.platform_properties._android_model},
                              {property_name : 'Network Carrier',value : col.platform_properties._network_carrier},
                              {property_name : 'Network Type',value : col.platform_properties._network_type},
                              {property_name : 'Style Type',value : col.event_properties.Style_Type},
                              {property_name : 'Dress Type',value : col.event_properties.Dress_Type},
                              {property_name : 'Card Name',value : col.event_properties.Card_Name},
                              {property_name : 'SD value',value : col.event_properties.SD_value},
                              {property_name : 'Rental Period',value : col.event_properties.Rental_Period},
                              {property_name : 'Delivery Date',value : col.event_properties.Delivery_Date},
                              {property_name : 'Product IDs',value : col.event_properties.Product_IDs},
                              {property_name : 'Category',value : col.event_properties.Category},
                              {property_name : 'Price',value : col.event_properties.Price},
                              {property_name : 'Brand',value : col.event_properties.Brand},
                              {property_name : 'Size',value : col.event_properties.Size},
                              {property_name : 'Description',value : col.event_properties.Description},
                              {property_name : 'Product ID',value : col.event_properties.Product_ID},
                              {property_name : 'Pick Up Date',value : col.event_properties.Pick_Up_Date},
                              {property_name : 'Filter Detail',value : col.event_properties.Filter_Detail},
                              {property_name : 'Rental Value',value : col.event_properties.Rental_Value},
                              {property_name : 'Rental_Value',value : col.event_properties.Rental_Value},
                              {property_name : 'Security Deposit',value : col.event_properties.Security_Deposit}
                        ];

                        /*col.properties = {
                              'User ID' : col.user_properties.Social_id,
                              'Name' : col.user_properties.Name,
                              'First Name' : col.user_properties.First_name,
                              'Last Name' : col.user_properties.Last_name,
                              'Gender' : col.user_properties.Gender,
                              'City' : col.user_properties.City,
                              'Email' : col.user_properties.Email,
                              'Mobile' : col.user_properties.Mobile,
                              'Channel' : col.user_properties.Channel,
                              'Flyrobe Analytic version' : col.platform_properties._flyrobe_analytics_version,
                              'Android OS name' : col.platform_properties._android_os_name,
                              'Android OS version' : col.platform_properties._android_os_version,
                              'Android Manufacturer' : col.platform_properties._android_manufacturer,
                              'Android Brand' : col.platform_properties._android_brand,
                              'Android Model' : col.platform_properties._android_model,
                              'Network Carrier' : col.platform_properties._network_carrier,
                              'Network Type' : col.platform_properties._network_type,
                              'Style Type' : col.event_properties.Style_Type,
                              'Dress Type' : col.event_properties.Dress_Type,
                              'Card Name' : col.event_properties.Card_Name,
                              'SD value' : col.event_properties.SD_value,
                              'Rental Period' : col.event_properties.Rental_Period,
                              'Delivery Date' : col.event_properties.Delivery_Date,
                              'Product IDs' : col.event_properties.Product_IDs,
                              'Category' : col.event_properties.Category,
                              'Price' : col.event_properties.Price,
                              'Brand' : col.event_properties.Brand,
                              'Size' : col.event_properties.Size,
                              'Description' : col.event_properties.Description,
                              'Product ID' : col.event_properties.Product_ID,
                              'Pickup Date' : col.event_properties.Pick_Up_Date,
                              'Filter Detail' : col.event_properties.Filter_Detail,
                              'Rental Value' : col.event_properties.Rental_Value,
                              'Rental' : col.event_properties.Rental,
                              'Security Deposit' : col.event_properties.Security_Deposit,
                        };*/
                  });

                  console.log( $scope.data);
                  $timeout(function () {
                        $('.table').trigger('footable_redraw');
                  }, 100);

            }, function errorCallback(response) {
                  mainServices.checkErrorStatus(response.status);
            });

      }



}

angular
      .module('Flyrobe')
      .controller('liveEventsCtrl', liveEventsCtrl);
