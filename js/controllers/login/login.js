/*controller for login to dashboard*/
/*later we have to add ACL login system*/

function loginCtrl($state, $timeout, $localStorage, $http, $scope,$rootScope) {


   
    var login = this;
    login.spinner = false;
    login.account = {};


    login.loginToDashboard = loginToDashboard; //function to login into dashboard


    function loginToDashboard() {

        login.spinner = true;

        $http({
            method: 'POST',
            url: mainVm.urlList.acl_base + '/user-login',
            headers: { 'Content-Type': 'application/json' },
            data: {
                "grant_type": 'password',
                "username": login.account.email,
                "password": login.account.password,
                "resource": 'PND'
            }

        }).success(function(res){

            login.spinner = false;
            if(!res.is_error){

                $localStorage.PND = {
                    email : login.account.email
                };
                mainVm.email = login.account.email;
                window.localStorage.setItem('pnd_token',res.data.user_info.access_token);
                window.localStorage.setItem('roles', JSON.stringify(res.data.user_role_based_access[0].resource_access_permissions));
                $rootScope.roles = res.data.user_role_based_access[0].resource_access_permissions;
                mainVm.roles = $rootScope.roles;
                $state.go('home.liveEvents');

            }
            else{
                if (res.status == 101) {
                    login.error = res.message;
                }
                else{
                    login.error = "Invalid Credebtials";
                }

                $timeout(function(){
                    login.error = "";
                },3000);
            }
        });
    }

}

angular
    .module('Flyrobe')
    .controller('loginCtrl', loginCtrl);