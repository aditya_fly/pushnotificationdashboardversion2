/**Function to control modal window */
function ModalInstanceCtrl($rootScope, $uibModalInstance) {

	mainVm.checkCredentials();

	$rootScope.ok = function () {
		$uibModalInstance.close();
	};

	$rootScope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

};
angular
	.module('Flyrobe')
	.controller('ModalInstanceCtrl', ModalInstanceCtrl);