function emailPreviewModalCtrl($state, $timeout,$window, $uibModal, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices, codeService) {

    mainVm.checkCredentials();

    $scope.editorOptions1 = {mode: 'text/html',
    lineNumbers: true,
    matchBrackets: true};

    $scope.html =$rootScope.content_email;

    $scope.$watch('html', function () { codeService.render($scope.html, $scope.css); }, true);
    $scope.$watch('css', function () { codeService.render($scope.html, $scope.css); }, true);


};

angular
	.module('Flyrobe')
	.controller('emailPreviewModalCtrl', emailPreviewModalCtrl);
