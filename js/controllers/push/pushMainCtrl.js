/*controller for displaying saved pushNotification template */

function pushMainCtrl($state, $timeout, $localStorage,$uibModal, $http, $scope, $rootScope, $log, mainServices) {

	pushVm = this;
	mainVm.checkCredentials();

	$localStorage.pn_state = "PN";
	mainVm.pn_state = $localStorage.pn_state;


	$scope.p1 = mainVm.roles[0].panels.PUSH.P1
	$scope.p2 = mainVm.roles[0].panels.PUSH.P2

	$scope.CurrentDate = new Date();
	$scope.loading = true;

	$scope.users = [];

	pushVm.test_input = "";

	/*$scope.searchThrough = () => {
		if(pushVm.test_input != ""){
			$scope.data = $scope.filter_data.filter(p => {
				return p.template_name.includes(pushVm.test_input)
			})
		}
	}*/

	$http({
		method: 'GET',
		url: mainVm.urlList.node_url2 + 'get_pn_templates/?page=1&limit=15', // No need of IP address
		headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
	}).then(function successCallback(response) {
		$scope.loading = false;
		$scope.filter_data = response.data.data;
		$scope.data = response.data.data;

		$timeout(function () {
			$('.table').trigger('footable_redraw');
		}, 100);

	}, function errorCallback(response) {
		mainServices.checkErrorStatus(response.status);
	});

	/*$scope.exportData = function () {
		alasql('SELECT * INTO XLSX("liveEvents' + $scope.CurrentDate + '.xlsx",{headers:true}) FROM ?', [$scope.data]);
	};*/

	$scope.setelectedUser = function (user) {
		$rootScope.selectedUser = user;
		$rootScope.cloneTemplateName = "Clone of " + user.template_name;
	}

	/*Function to send the saved template */
	/*$scope.sendSavedTemp = function (user) {
		//alert('I click');
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url1 + 'firebase_push',
			data: {
				template_name: user.template_name,
				email: user.template_content.email,
				template_type: user.template_content.template_type,
				triggered_from: user.template_content.triggered_from,
				segment_name: user.template_content.segment_name,

				content: {
					title: user.template_content.content.title,
					body: user.template_content.content.body,
					deeplink: user.template_content.content.deeplink,
					big_image: user.template_content.content.big_image,
					large_icon: user.template_content.content.large_icon,
				}

			},
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {

			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			}
			else if (response.data.status == -1) {
				alert("internet connection failed");
			}
			else if (response.data.status == 200) {
				alert("Successfully sent :: " + response.data.data);
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};*/

	/*$scope.getPushPreivew =function(user) {
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_pn_templates/?page=1&limit=15', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;
	
			$scope.segmentArr = response.data.data;
            for (var i = 0; i < $scope.segmentArr.length; i++) {
                if (($scope.segmentArr[i].template_name) === (user.template_name)) {
					$rootScope.title_push= $scope.segmentArr[i].template_content.content.title;
					$rootScope.body_push= $scope.segmentArr[i].template_content.content.body;
					$rootScope.big_image_push= $scope.segmentArr[i].template_content.content.big_image;
					$rootScope.large_icon_push= $scope.segmentArr[i].template_content.content.large_icon;
					$rootScope.template_type_push= $scope.segmentArr[i].template_content.template_type;
					
                }
            }
	
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});
	};*/

	$scope.openPushPreview = function (user) {


		for (var i = 0; i < $scope.data.length; i++) {
			if (($scope.data[i].template_name) === (user.template_name)) {
				$rootScope.title_push= $scope.data[i].template_content.content.title;
				$rootScope.body_push= $scope.data[i].template_content.content.body;
				$rootScope.big_image_push= $scope.data[i].template_content.content.big_image;
				$rootScope.large_icon_push= $scope.data[i].template_content.content.large_icon;
				$rootScope.template_type_push= $scope.data[i].template_content.template_type;

			}
		}

		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/pushPreviewModal.html',
			controller: ModalInstanceCtrl
		});
	};

};
angular
	.module('Flyrobe')
	.controller('pushMainCtrl', pushMainCtrl);
