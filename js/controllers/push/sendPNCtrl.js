/**controller for sending pushNotification */

function sendPNCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $uibModal, $window, mainServices) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "PN";
	mainVm.pn_state = $localStorage.pn_state;

	$rootScope.userData = {};
	$rootScope.userArr = [
		// { id: 1, user: 'All Users' },
		{ id: 2, user: 'Selected Users' },
		{ id: 3, user: 'Segments' }
	];

	/**Function to validate form */
	$scope.validateForm = function () {
		if ($scope.userData.selection == "" || $scope.userData.selection == null || $scope.userData.selection == undefined) {
			alert("Please select and fill the desired fields");
		}

		$rootScope.template_name = $scope.userData.template_Name;
		$rootScope.template_type = $scope.userData.selection;
		$rootScope.title = $scope.userData.title;
		$rootScope.body = $scope.userData.body;
		$rootScope.deeplink = $scope.userData.deepLink;
		$rootScope.big_image = $scope.userData.bigImage;
		$rootScope.large_icon = $scope.userData.largeIcon;
	}

	/**Function to open model */
	$scope.open1 = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/modalWindow.html',
			controller: ModalInstanceCtrl
		});
	};

	/**Function to update template */
	$scope.saveTemp = function () {
		//alert('I click');
		if ($scope.userData.selection == "" || $scope.userData.selection == null || $scope.userData.selection == undefined) {
			alert("Please select and fill the desired fields");
		}
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'save_pn_templates/',
			data: {
				template_name: $scope.userData.template_Name,
				number_of_user: 0,
				template_content: {
					template_type: $scope.userData.selection,
					triggered_from: "dashboard",
					segment_name: "test_segment1",

					content: {
						title: $scope.userData.title,
						body: $scope.userData.body,
						deeplink: $scope.userData.deepLink,
						big_image: $scope.userData.bigImage,
						large_icon: $scope.userData.largeIcon
					}
				}

			},
			headers: { 'Content-Type': 'application/json', 'Authorization': '1nhf2f48djh05mnd8' }
		}).then(function successCallback(response) {

			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			}
			else if (response.data.status == 200) {
				alert("Successfully save :: " + response.data.data);
				$window.location.href = '#/dashboards/pushMain';
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');
	};


};

angular
	.module('Flyrobe')
	.controller('sendPNCtrl', sendPNCtrl);




