/**controller to update saved template */
function updateSavedTempCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $uibModal, $window, mainServices,
							 $stateParams) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "PN";
	mainVm.pn_state = $localStorage.pn_state;

	$scope.spinner = true;

	$rootScope.userData = {};
	$rootScope.userArr = [
		// { id: 1, user: 'All Users' },
		{ id: 2, user: 'Selected Users' },
		{ id: 3, user: 'Segments' }
	];


	$scope.getTemplate = function() {

		var name = $stateParams.template_name;

		var tname = name.replace(/%20/g, " ");
		console.log(tname);


		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_pn_templates/' + tname,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).success(function(response) {
			$scope.spinner = false;

			$scope.selectedUser = response.data[0];
			$scope.validateForm();

		});

	}

	$scope.getTemplate();//Default Call

	/**Function to validate form */
	$scope.validateForm = function () {
		if ($scope.selectedUser.template_content.template_type == "" || $scope.selectedUser.template_content.template_type == null || $scope.selectedUser.template_content.template_type == undefined) {
			alert("Please select and fill the desired fields");
		}

		$rootScope.template_name = $scope.selectedUser.template_name;
		$rootScope.template_type = $scope.selectedUser.template_content.template_type;
		$rootScope.title = $scope.selectedUser.template_content.content.title;
		$rootScope.body = $scope.selectedUser.template_content.content.body;
		$rootScope.deeplink = $scope.selectedUser.template_content.content.deeplink;
		$rootScope.big_image = $scope.selectedUser.template_content.content.big_image;
		$rootScope.large_icon = $scope.selectedUser.template_content.content.large_icon;
	}

	/**Function to open model */
	$scope.open1 = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/modal/modalWindow.html',
			controller: ModalInstanceCtrl
		});
	};

	/**Function to update template */

	$scope.updateTemp = function () {
		//alert($scope.selectedUser.template_name);
		// alert($scope.selectedUser.template_content.email);
		// alert($scope.selectedUser.template_name);
		if ($scope.selectedUser.template_content.template_type == "" || $scope.selectedUser.template_content.template_type == null || $scope.selectedUser.template_content.template_type == undefined) {
			alert("Please select and fill the desired fields");
		}


		$http({
			method: 'PUT',
			url: mainVm.urlList.node_url2 + 'update_pn_template/' + $scope.selectedUser.template_name,
			data: {
				"template_name": $scope.selectedUser.template_name,
				"number_of_user": 0,
				"template_content": {
					"template_type": $scope.selectedUser.template_content.template_type,
					"triggered_from": "dashboard",
					"segment_name": "test_segment1",

					"content": {
						"title": $scope.selectedUser.template_content.content.title,
						"body": $scope.selectedUser.template_content.content.body,
						"deeplink": $scope.selectedUser.template_content.content.deeplink,
						"big_image": $scope.selectedUser.template_content.content.big_image,
						"large_icon": $scope.selectedUser.template_content.content.large_icon
					}
				}

			},
			headers: { 'Content-Type': 'application/json', 'Authorization': '1nhf2f48djh05mnd8' }
		}).then(function successCallback(response) {

			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			}
			else if (response.data.status == 200) {

				alert("Successfully updated");
				$window.location.href = '#/dashboards/pushMain';

			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');




	};


};

angular
	.module('Flyrobe')
	.controller('updateSavedTempCtrl', updateSavedTempCtrl);





