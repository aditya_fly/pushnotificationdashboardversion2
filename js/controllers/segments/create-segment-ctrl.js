/*controller to create segment */

function createSegmentCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices,
                           commonService) {

    this.tab = 1;
    this.spinner = false;

    $scope.segmentData = {
        recent_activity  : {},
        lifetime_activity : {},
        demographic_activity : {}
    };

    mainVm.checkCredentials();
    $localStorage.pn_state = "Create Segment";
    mainVm.pn_state = $localStorage.pn_state;

    $scope.fieldArr = ['Lifetime_revenue', 'Lifetime_visits', 'Lifetime_order'];
    $scope.conditiondArr = ['less_than', 'equal_to', 'greater_than'];


    this.getEventList		= getEventList;
    this.getLocationList	= getLocationList;

    this.getEventList();//Default Call
    this.getLocationList();//Default Call




    this.selectTab = function (setTab,type) {


        if(type == 'SAVE' || type == 'REMOVE'){

            send_data = {};

            if(type == 'SAVE'){

                if(setTab == 1){
                    if($scope.segmentData.recent_activity.start_date){
                        $scope.start_date = $filter('date')($scope.segmentData.recent_activity.start_date, 'yyyy-MM-dd');
                    }

                    if($scope.segmentData.recent_activity.end_date){
                        $scope.end_date = $filter('date')($scope.segmentData.recent_activity.end_date, 'yyyy-MM-dd');
                    }

                    if(($scope.segmentData.recent_activity.event_name == '' || $scope.segmentData.recent_activity.event_name == null || $scope.segmentData.recent_activity.event_name ==undefined) ||
                        ($scope.start_date == '' || $scope.start_date == null || $scope.start_date == undefined) ||
                        ($scope.end_date == '' || $scope.end_date == null || $scope.end_date == undefined)){
                        alert("Please select all required fields");
                        return false;
                    }

                    $scope.recent = true;
                }

                if(setTab == 2){
                    if(($scope.segmentData.lifetime_activity.field_name == '' || $scope.segmentData.lifetime_activity.field_name == null || $scope.segmentData.lifetime_activity.field_name ==undefined)
                        || ($scope.segmentData.lifetime_activity.condition_name == '' || $scope.segmentData.lifetime_activity.condition_name == null || $scope.segmentData.lifetime_activity.condition_name ==undefined)
                        || ($scope.segmentData.lifetime_activity.value == '' || $scope.segmentData.lifetime_activity.value == null || $scope.segmentData.lifetime_activity.value ==undefined)){
                        alert("Please select all required fields");
                        return false;
                    }
                    $scope.lifetime = true;
                }

                if(setTab == 3){
                    if(($scope.segmentData.demographic_activity.gender == '' || $scope.segmentData.demographic_activity.gender == null || $scope.segmentData.demographic_activity.gender == undefined) &&
                        ($scope.segmentData.demographic_activity.location == '' || $scope.segmentData.demographic_activity.location == null || $scope.segmentData.demographic_activity.location == undefined || $scope.segmentData.demographic_activity.location.length <= 0)){
                        alert("Please select atleast one field");
                        return false;
                    }
                    $scope.demographic = true;
                }

                if(setTab != 3){


                    this.tab = setTab + 1;
                }

            }


            /******* START recent_activity ********/
            if($scope.recent){
                if($scope.segmentData.recent_activity.event_name != '' && $scope.segmentData.recent_activity.event_name != null
                    && $scope.segmentData.recent_activity.event_name !=undefined){
                    send_data.recent_activity = {
                        event_name: $scope.segmentData.recent_activity.event_name,
                        date_range: [$scope.start_date, $scope.end_date]
                    };
                }
            }
            /******* END recent_activity ********/


            /******* START lifetime_activity ********/
            if($scope.lifetime){

                if($scope.segmentData.lifetime_activity.field_name != '' && $scope.segmentData.lifetime_activity.field_name != null && $scope.segmentData.lifetime_activity.field_name !=undefined){
                    if(!send_data.lifetime_activity){
                        send_data.lifetime_activity = [{}];
                    }
                    send_data.lifetime_activity[0].field_name = $scope.segmentData.lifetime_activity.field_name;
                }
                if($scope.segmentData.lifetime_activity.condition_name != '' && $scope.segmentData.lifetime_activity.condition_name != null && $scope.segmentData.lifetime_activity.condition_name !=undefined){
                    if(!send_data.lifetime_activity){
                        send_data.lifetime_activity = [{}];
                    }
                    send_data.lifetime_activity[0].condition_name = $scope.segmentData.lifetime_activity.condition_name;
                }
                if($scope.segmentData.lifetime_activity.value != '' && $scope.segmentData.lifetime_activity.value != null && $scope.segmentData.lifetime_activity.value !=undefined){
                    if(!send_data.lifetime_activity){
                        send_data.lifetime_activity = [{}];
                    }
                    send_data.lifetime_activity[0].value = parseInt($scope.segmentData.lifetime_activity.value);
                }
            }
            /******* END lifetime_activity ********/


            /******* START demographic_activity ********/
            if($scope.demographic){

                if($scope.segmentData.demographic_activity.gender != '' && $scope.segmentData.demographic_activity.gender != null && $scope.segmentData.demographic_activity.gender !=undefined){
                    if(!send_data.demographic_activity){
                        send_data.demographic_activity = {};
                    }
                    send_data.demographic_activity.gender = $scope.segmentData.demographic_activity.gender;
                }

                if($scope.segmentData.demographic_activity.location != '' && $scope.segmentData.demographic_activity.location != null && $scope.segmentData.demographic_activity.location !=undefined &&
                    $scope.segmentData.demographic_activity.location.length > 0){
                    if(!send_data.demographic_activity){
                        send_data.demographic_activity = {};
                    }
                    send_data.demographic_activity.location_name = $scope.segmentData.demographic_activity.location;
                }
            }
            /******* END demographic_activity ********/





            $http({
                method: 'POST',
                url: mainVm.urlList.node_url2 + 'get_segment_data/',
                data: send_data,
                headers: mainVm.header1
            }).then(function successCallback(response) {

                if(response.data.is_error){
                    $scope.segment_content = {
                        segment_email : [],
                        email_enable_count : 0,
                        push_enable_count : 0,
                        sms_enable_count : 0
                    };
                }
                else{
                    $scope.segment_content  = response.data.data;
                }


            }, function errorCallback(response) {
                mainServices.checkErrorStatus(response.status);
            });
        }
        else{
            this.tab = setTab;
        }





    };

    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    }


    /**Function for validating the for to create segment */
    $scope.validateForm = function () {

        if ($scope.segmentData.segment_Name == "" || $scope.segmentData.segment_Name == null || $scope.segmentData.segment_Name == undefined) {
            alert("Please give the segment name");
            return false;
        }

        if($scope.segmentData.recent_activity.start_date){
            $scope.start_date = $filter('date')($scope.segmentData.recent_activity.start_date, 'yyyy-MM-dd');
        }

        if($scope.segmentData.recent_activity.end_date){
            $scope.end_date = $filter('date')($scope.segmentData.recent_activity.end_date, 'yyyy-MM-dd');
        }

        var send_data = {
            segment_name : $scope.segmentData.segment_Name
        };

        /******* START recent_activity ********/
        if($scope.recent){
            if($scope.segmentData.recent_activity.event_name != '' && $scope.segmentData.recent_activity.event_name != null && $scope.segmentData.recent_activity.event_name !=undefined){

                if($scope.start_date == '' || $scope.start_date == null || $scope.start_date == undefined){
                    alert("Please select Start Date");
                    return false;
                }

                if($scope.end_date == '' || $scope.end_date == null || $scope.end_date == undefined){
                    alert("Please select End Date");
                    return false;
                }

                send_data.recent_activity = {
                    event_name: $scope.segmentData.recent_activity.event_name,
                    date_range: [$scope.start_date, $scope.end_date],
                };
            }
        }
        /******* END recent_activity ********/

        /******* START demographic_activity ********/
        if($scope.demographic){
            if($scope.segmentData.demographic_activity.gender != '' && $scope.segmentData.demographic_activity.gender != null && $scope.segmentData.demographic_activity.gender !=undefined){
                if(!send_data.demographic_activity){
                    send_data.demographic_activity = {};
                }
                send_data.demographic_activity.gender = $scope.segmentData.demographic_activity.gender;
            }

            if($scope.segmentData.demographic_activity.location != '' && $scope.segmentData.demographic_activity.location != null && $scope.segmentData.demographic_activity.location !=undefined &&
                $scope.segmentData.demographic_activity.location.length > 0){
                if(!send_data.demographic_activity){
                    send_data.demographic_activity = {};
                }
                send_data.demographic_activity.location_name = $scope.segmentData.demographic_activity.location;
            }
        }
        /******* END demographic_activity ********/

        /******* START lifetime_activity ********/
        if($scope.lifetime){
            if($scope.segmentData.lifetime_activity.field_name != '' && $scope.segmentData.lifetime_activity.field_name != null && $scope.segmentData.lifetime_activity.field_name !=undefined){
                if(!send_data.lifetime_activity){
                    send_data.lifetime_activity = [{}];
                }
                send_data.lifetime_activity[0].field_name = $scope.segmentData.lifetime_activity.field_name;
            }
            if($scope.segmentData.lifetime_activity.condition_name != '' && $scope.segmentData.lifetime_activity.condition_name != null && $scope.segmentData.lifetime_activity.condition_name !=undefined){
                if(!send_data.lifetime_activity){
                    send_data.lifetime_activity = [{}];
                }
                send_data.lifetime_activity[0].condition_name = $scope.segmentData.lifetime_activity.condition_name;
            }
            if($scope.segmentData.lifetime_activity.value != '' && $scope.segmentData.lifetime_activity.value != null && $scope.segmentData.lifetime_activity.value !=undefined){
                if(!send_data.lifetime_activity){
                    send_data.lifetime_activity = [{}];
                }
                send_data.lifetime_activity[0].value = parseInt($scope.segmentData.lifetime_activity.value);
            }
        }
        /******* END lifetime_activity ********/

        if(commonService.emptyObject($scope.segmentData.lifetime_activity) && commonService.emptyObject($scope.segmentData.demographic_activity)
        && commonService.emptyObject($scope.segmentData.recent_activity)){
            alert("Please select atleast one activity.");
            return false;
        }

        this.spinner = true;

        $http({
            method: 'POST',
            url: mainVm.urlList.node_url2 + 'create_segmentation/',
            data: send_data,
            headers: mainVm.header1
        }).then(function successCallback(response) {

            this.spinner = false;

            if (response.data.is_error == true) {
                alert("Oops :: " + response.data.message);
            }

            else if (response.data.status == 200) {
                alert("Successfully completed :: ");
                $state.go('table.segments');
            }
            $scope.loading = false;
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });

    };

    /**Function to get event_list */
    function getEventList() {
        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_event_list/Android', // No need of IP address
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).success(function (response) {
            $scope.eventArr = response.data;
            this.spinner = false;

        }).error(function (response) {
            mainServices.checkErrorStatus(response.status);
        });
    }

    /**Function to get list of city(location) */
    function getLocationList() {
        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_city_list/', // No need of IP address
            headers: mainVm.header2
        }).then(function successCallback(response) {
            $scope.locationArr = [];

            response.data.data.forEach(function (col) {
                if(col.length){
                    $scope.locationArr.push(col);
                }
            });

        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });
    }

    /*$scope.checkLocation = function () {

        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_field_list/?key_name=user_properties.City', // No need of IP address
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {
            $scope.locationArr = response.data.data;

        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });

    };*/

}

angular
    .module('Flyrobe')
    .controller('createSegmentCtrl', createSegmentCtrl);
