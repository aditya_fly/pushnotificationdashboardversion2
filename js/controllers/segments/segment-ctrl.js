//controller for saved segment page

function segmentsMainCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $log, mainServices) {

    svm = this;
    svm.spinner = true;

    mainVm.checkCredentials();

    $localStorage.pn_state = "Segment";
    mainVm.pn_state = $localStorage.pn_state;

    svm.getTableData        = getTableData;

    svm.getTableData();//Default Call



    $scope.p1 = mainVm.roles[0].panels.SEG.P1;
    $scope.p2 = mainVm.roles[0].panels.SEG.P2;

    $scope.search_input = "";

    function getTableData() {
        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_segments/?page=1&limit=15',
            headers: mainVm.header1
        }).then(function successCallback(response) {

            svm.spinner = false;
            $scope.data_to_filter = response.data.data;
            svm.table_data = response.data.data;

            $timeout(function () {
                $('.table').trigger('footable_redraw');
            }, 100);

        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });
    }


    /*undefined function */
    $scope.sendSavedTemp = function (user) {
        //alert('I click');
        $http({
            method: 'POST',
            url: mainVm.urlList.node_url1 + 'firebase_push',
            data: {
                template_name: user.template_name,
                email: user.template_content.email,
                template_type: user.template_content.template_type,
                triggered_from: user.template_content.triggered_from,
                segment_name: user.template_content.segment_name,

                content: {
                    title: user.template_content.content.title,
                    body: user.template_content.content.body,
                    deeplink: user.template_content.content.deeplink,
                    big_image: user.template_content.content.big_image,
                    large_icon: user.template_content.content.large_icon,
                }

            },
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {

            if (response.data.is_error == true) {

                alert("Oops :: " + response.data.message);
            }
            else if (response.data.status == -1) {
                alert("internet connection failed");
            }
            else if (response.data.status == 200) {
                alert("Successfully sent :: " + response.data.data);
            }
            $scope.loading = false;
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });
    };

};
angular
    .module('Flyrobe')
    .controller('segmentsMainCtrl', segmentsMainCtrl);
