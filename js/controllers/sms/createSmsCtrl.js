/*controller to create segment */

function createSmsCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $uibModal, $filter, $log, mainServices) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "Create SMS";
	mainVm.pn_state = $localStorage.pn_state;

	/**Function for save sms*/
	$scope.saveSMS = function (type) {
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'save_sms_templates/',
			data: {
                "template_name": $scope.sms_template_name,
	            "template_content": $scope.sms_template_content

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			if (response.data.is_error == true) {
				alert("Oops :: " + response.data.message);
			} else {

				if(type == 'save'){
					var modalInstance = $uibModal.open({
						template: '<h1>Succesfully Added</h1>'
					});
					$state.go('home.sms')
				}
				else{
					$scope.open1();//open modal to send sms
				}

			}

			
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

	};


	/**Function to open model */
	$scope.open1 = function () {

		$rootScope.template_name = $scope.sms_template_name;
		$rootScope.template_content = $scope.sms_template_content;

		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/sms/modalWindowSms.html',
			controller: ModalInstanceCtrl
		});
	};


};

angular
	.module('Flyrobe')
	.controller('createSmsCtrl', createSmsCtrl);
