function modalWindowSmsCtrl($state, $timeout, $localStorage, $http, $scope, $rootScope, $uibModal, $window, mainServices) {

    mainVm.checkCredentials();

    $scope.userArr = [
        // { id: 1, user: 'All Users' },
        { id: 2, user: 'Selected Users' },
        { id: 3, user: 'Segments' }
    ];

    $scope.segmentName = [];
    $scope.PnList = [];

    /*Function to get email id of all users and list of all segment*/
    $scope.check = function () {
        // console.log('userSelected',$scope.userData.userSelected);
        if ($scope.userData.userSelected.id == 1) {
            // console.log("Selected user is All");
            $http({
                method: 'GET',
                url: mainVm.urlList.node_url2 + 'all_user_email', // No need of IP address
                headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
            }).then(function successCallback(response) {
                // console.log("Data",response.data.data);
                $scope.ss = response.data.data;

            }, function errorCallback(response) {
                mainServices.checkErrorStatus(response.status);
                // console.log("Failure");
            });

        }
        else if ($scope.userData.userSelected.id == 2) {
            // console.log("Entered in selected users");

        }
        else if ($scope.userData.userSelected.id == 3) {
            // console.log("Entered in Event users");
            $http({
                method: 'GET',
                url: mainVm.urlList.node_url2 + 'get_segments/', // No need of IP address
                headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_1 }
            }).then(function successCallback(response) {
                //console.log("Data",response.data.data);
                $scope.segmentArr = response.data.data;
                console.log( $scope.segmentArr);
                for (var i = 0; i < $scope.segmentArr.length; i++) {
                    $scope.segmentName.push($scope.segmentArr[i].segment_name)
                }

                console.log($scope.segmentname);
                // console.log("Hey",$scope.eventArr);

            }, function errorCallback(response) {
                mainServices.checkErrorStatus(response.status);
                // console.log("Failure");
            });

        }
        else {

        }
    };

    /*Function to get email id of particular segemnt */
    $scope.checkEvent = function () {
        // console.log("Entered into the Check Event",$scope.userData.eventArr);
        $scope.Android = $scope.userData.eventArr;

        // console.log("Android ::::", $scope.Android);
        $http({
            method: 'GET',
            url: mainVm.urlList.node_url2 + 'get_segments/', // No need of IP address
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_1 }
        }).then(function successCallback(response) {
            $scope.segmentArr = response.data.data;
            for (var i = 0; i < $scope.segmentArr.length; i++) {
                if (($scope.segmentArr[i].segment_name) === ($scope.Android)) {
                    $scope.PnList = $scope.segmentArr[i].segment_mobile;
                }
            }
            // console.log("Data",response.data.data);
            // console.log("Android ::::", $scope.Android);
            $scope.ss = $scope.PnList;
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
            // console.log("Failure");
        });


    };


   /**Function for save sms*/
	$scope.saveSms = function () {
		$http({
			method: 'POST',
			url: mainVm.urlList.node_url2 + 'save_sms_templates/',
			data: {
                "template_name": $rootScope.template_name,
	            "template_content": $rootScope.template_content

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {


			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			}

			else if (response.data.status == 200) {
				// $scope.open1();

				alert("Successfully completed :: ");
				 $window.location.href = '#/dashboards/sms';
			}
			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');

    };
    
    /**Function to send sms */
    $scope.sendSms = function () {
        if ($scope.userData.userSelected.id == 2) {
            $scope.s = $scope.userData.phoneNo
            $scope.ss = $scope.s.split(",");
        }
        $http({
            method: 'POST',
            url: mainVm.urlList.node_url2 + 'send_sms/',
            data: {
                "mobile":$scope.ss,
                "content":$rootScope.template_content,

            },
            headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
        }).then(function successCallback(response) {


            if (response.data.is_error == true) {

                alert("Oops :: " + response.data.message);
            }

            else if (response.data.status == 200) {
                // $scope.open1();

                swal("Successfully sent");
                $window.location.href = '#/dashboards/sms';
            }
            $scope.loading = false;
        }, function errorCallback(response) {
            mainServices.checkErrorStatus(response.status);
        });

        // console.log('user form is in scope');



        this.userData = {};
    };
   


};

angular
    .module('Flyrobe')
    .controller('modalWindowSmsCtrl', modalWindowSmsCtrl);
