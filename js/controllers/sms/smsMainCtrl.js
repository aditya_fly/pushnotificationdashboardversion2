/*controller to create campaigns */

function smsMainCtrl($state, $timeout, $uibModal, $localStorage, $http, $scope, $rootScope, $filter, $log, mainServices) {

	smsVm = this;
	mainVm.checkCredentials();
	$localStorage.pn_state = "SMS";
	mainVm.pn_state = $localStorage.pn_state;

	smsVm.search_input = ""

	smsVm.search_input = ""
	/*$scope.searchThrough = () => {
		$scope.data = $scope.filter_data.filter(p => {
			return p.template_name.toLowerCase().includes(smsVm.search_input)
		})
	}*/


	$scope.p1 = mainVm.roles[0].panels.SMS.P1
	$scope.p2 = mainVm.roles[0].panels.SMS.P2

	$scope.loading = true;
	/**Function to get list of sms template*/
		
		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_sms_templates/?page=1&limit=15', // No need of IP address
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {
			$scope.loading = false;
			$scope.filter_data = response.data.data;
			$scope.data = response.data.data;		
			$timeout(function () {
				$('.table').trigger('footable_redraw');
			}, 100);

		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});


		$scope.setelectedSmsTemplate = function (Sms) {
			$rootScope.setelectedSms = Sms;
			$rootScope.cloneSmsTemplateName = "Clone of " + Sms.template_name;
		}
	

	
};

angular
	.module('Flyrobe')
	.controller('smsMainCtrl', smsMainCtrl);
