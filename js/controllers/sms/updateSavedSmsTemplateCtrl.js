/*controller to create segment */

function updateSavedSmsTemplateCtrl($state, $timeout,$window, $uibModal, $localStorage, $http, $scope,
									$rootScope, $filter, $log, mainServices,$stateParams) {

	mainVm.checkCredentials();
	$localStorage.pn_state = "SMS";
	mainVm.pn_state = $localStorage.pn_state;


	$scope.getTemplate = function() {

		var name = $stateParams.template_name;

		var tname = name.replace(/%20/g, " ");
		console.log(tname);


		$http({
			method: 'GET',
			url: mainVm.urlList.node_url2 + 'get_sms_templates/' + tname,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).success(function(response) {
			$scope.spinner = false;

			$scope.setelectedSms = response.data[0];
			$scope.validateForm();

		});

	}

	$scope.getTemplate();//Default Call



    /**Function for updation of sms*/
	$scope.updateSms = function (type) {
		$http({
			method: 'PUT',
			url: mainVm.urlList.node_url2 + 'update_sms_template/' +$scope.setelectedSms.template_name,
			data: {
            
	            "template_content": $scope.setelectedSms.template_content

			}
			,
			headers: { 'Content-Type': 'application/json', 'Authorization': mainVm.AuthorizationHeaderList.Authorization_2 }
		}).then(function successCallback(response) {


			if (response.data.is_error == true) {

				alert("Oops :: " + response.data.message);
			} else {

				if(type == 'update'){
					var modalInstance = $uibModal.open({
						template: '<h1>Succesfully updated</h1>',
					});
					$state.go('home.sms')
				}
				else{
					$scope.open1();
				}


			}

			$scope.loading = false;
		}, function errorCallback(response) {
			mainServices.checkErrorStatus(response.status);
		});

		// console.log('user form is in scope');

	};

	/**Functiomn for sending sms */
	$scope.validateForm = function () {
		$rootScope.template_name = $scope.setelectedSms.template_name;
		$rootScope.template_content = $scope.setelectedSms.template_content;
		
	}
	/**Function to open model */
	$scope.open1 = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/pages/sms/modalWindowSms.html',
			controller: ModalInstanceCtrl
		});
	};


};

angular
	.module('Flyrobe')
	.controller('updateSavedSmsTemplateCtrl', updateSavedSmsTemplateCtrl);
