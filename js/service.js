/*service to control error of http response message*/
angular
    .module('Flyrobe')
    .service('mainServices', function($state) {

        this.checkErrorStatus = function (status) {

            if(status == 401){
                alert("Please login again");
                $state.go('login_two_columns');
                return false;
            }
            /*else if (status == -1) {
                alert("Please check your internet connection.");
                return false;
            }*/

            return true;

        };

    })
    .service('codeService', function () {
        this.render = function (html, css) {
          source = "<html><head><style>" + css + "</style></head>" + html +"</html>";
          
          var iframe = document.querySelector('#output iframe'),
              iframe_doc = iframe.contentDocument;
           
          iframe_doc.open();
          iframe_doc.write(source);
          iframe_doc.close();
        }
      })
