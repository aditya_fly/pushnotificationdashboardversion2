/**
 * Created by surbhi on 07/06/17.
 */
angular
    .module('Flyrobe')
    .service('commonService', function() {

        this.emptyObject = function (obj){
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }
            return true;
        };

        this.daysBetween = function (date1, date2) {

            console.log(date1);
            console.log(date2);

            //Get 1 day in milliseconds
            var one_day=1000*60*60*24;

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime();
            var date2_ms = date2.getTime();

            console.log(date1_ms);
            console.log(date2_ms);

            // Calculate the difference in milliseconds
            var difference_ms = date2_ms - date1_ms;

            // Convert back to days and return
            return Math.round(difference_ms/one_day);

        }

        this.timeBetween = function (date1, date2) {

            console.log(date1);
            console.log(date2);

            //Get 1 day in milliseconds
            var one_day=1000*60*60*24;

            // Convert both dates to milliseconds
            var date1_ms = date1.getTime();
            var date2_ms = date2.getTime();

            console.log(date1_ms);
            console.log(date2_ms);

            // Calculate the difference in milliseconds
            var difference_ms = date2_ms - date1_ms;

            // Convert back to days and return
            return Math.round(difference_ms);

        }


    });